# frozen_string_literal: true

require 'nokogiri'
require 'active_support/core_ext/enumerable'

module Gitlab
  module QA
    module Report
      # Uses the API to create or update GitLab issues with the results of tests from RSpec report files.
      class ResultsInIssues < ReportAsIssue
        MAX_TITLE_LENGTH = 255

        private

        def run!
          puts "Reporting test results in `#{files.join(',')}` as issues in project `#{project}` via the API at `#{Runtime::Env.gitlab_api_base}`."

          Dir.glob(files).each do |path|
            puts "Reporting tests in #{path}"
            extension = File.extname(path)

            case extension
            when '.json'
              test_results = Report::JsonTestResults.new(path)
            when '.xml'
              test_results = Report::JUnitTestResults.new(path)
            else
              raise "Unknown extension #{extension}"
            end

            test_results.each do |test|
              report_test(test)
            end

            test_results.write(path)
          end
        end

        def report_test(test)
          return if test.skipped

          puts "Reporting test: #{test.file} | #{test.name}"

          issue = find_issue(test)

          if issue
            puts "Found existing issue: #{issue.web_url}"
          else
            issue = create_issue(test)
            puts "Created new issue: #{issue.web_url}"
          end

          test.testcase ||= issue.web_url

          update_labels(issue, test)
          note_status(issue, test)

          puts "Issue updated"
        end

        def find_issue(test)
          title = title_from_test(test)
          issues =
            gitlab.find_issues(
              iid: iid_from_testcase_url(test.testcase),
              options: { search: search_term(test) }) do |issue|
                issue.state == 'opened' && issue.title.strip == title
              end

          warn(%(Too many issues found with the file path "#{test.file}" and name "#{test.name}")) if issues.many?

          issues.first
        end

        def create_issue(test)
          gitlab.create_issue(
            title: title_from_test(test),
            description: "### Full description\n\n#{search_safe(test.name)}\n\n### File path\n\n#{test.file}",
            labels: 'status::automated'
          )
        end

        def iid_from_testcase_url(url)
          url && url.split('/').last.to_i
        end

        def search_term(test)
          %("#{test.file}" "#{search_safe(test.name)}")
        end

        def title_from_test(test)
          title = "#{partial_file_path(test.file)} | #{search_safe(test.name)}".strip

          return title unless title.length > MAX_TITLE_LENGTH

          "#{title[0...MAX_TITLE_LENGTH - 3]}..."
        end

        def partial_file_path(path)
          path.match(/((api|browser_ui).*)/i)[1]
        end

        def search_safe(value)
          value.delete('"')
        end

        def note_status(issue, test)
          return if test.failures.empty?

          note = note_content(test)

          gitlab.handle_gitlab_client_exceptions do
            Gitlab.issue_discussions(project, issue.iid, order_by: 'created_at', sort: 'asc').each do |discussion|
              return add_note_to_discussion(issue.iid, discussion.id) if new_note_matches_discussion?(note, discussion)
            end

            Gitlab.create_issue_note(project, issue.iid, note)
          end
        end

        def note_content(test)
          errors = test.failures.each_with_object([]) do |failure, text|
            text << <<~TEXT
              Error:
              ```
              #{failure['message']}
              ```

              Stacktrace:
              ```
              #{failure['stacktrace']}
              ```
            TEXT
          end.join("\n\n")

          "#{failure_summary}\n\n#{errors}"
        end

        def failure_summary
          summary = [":x: ~\"#{pipeline}::failed\""]
          summary << "~\"quarantine\"" if quarantine_job?
          summary << "in job `#{Runtime::Env.ci_job_name}` in #{Runtime::Env.ci_job_url}"
          summary.join(' ')
        end

        def quarantine_job?
          Runtime::Env.ci_job_name&.include?('quarantine')
        end

        def new_note_matches_discussion?(note, discussion)
          note_error = error_and_stack_trace(note)
          discussion_error = error_and_stack_trace(discussion.notes.first['body'])

          return false if note_error.empty? || discussion_error.empty?

          note_error == discussion_error
        end

        def error_and_stack_trace(text)
          result = text.strip[/Error:(.*)/m, 1].to_s

          warn "Could not find `Error:` in text: #{text}" if result.empty?

          result
        end

        def add_note_to_discussion(issue_iid, discussion_id)
          gitlab.handle_gitlab_client_exceptions do
            Gitlab.add_note_to_issue_discussion_as_thread(project, issue_iid, discussion_id, body: failure_summary)
          end
        end

        def update_labels(issue, test)
          labels = issue.labels
          labels.delete_if { |label| label.start_with?("#{pipeline}::") }
          labels << (test.failures.empty? ? "#{pipeline}::passed" : "#{pipeline}::failed")
          labels << "Enterprise Edition" if ee_test?(test)
          quarantine_job? ? labels << "quarantine" : labels.delete("quarantine")

          gitlab.edit_issue(iid: issue.iid, options: { labels: labels })
        end

        def ee_test?(test)
          test.file =~ %r{features/ee/(api|browser_ui)}
        end

        def pipeline
          # Gets the name of the pipeline the test was run in, to be used as the key of a scoped label
          #
          # Tests can be run in several pipelines:
          #   gitlab-qa, nightly, master, staging, canary, production, preprod, and MRs
          #
          # Some of those run in their own project, so CI_PROJECT_NAME is the name we need. Those are:
          #   nightly, staging, canary, production, and preprod
          #
          # MR, master, and gitlab-qa tests run in gitlab-qa, but we only want to report tests run on master
          # because the other pipelines will be monitored by the author of the MR that triggered them.
          # So we assume that we're reporting a master pipeline if the project name is 'gitlab-qa'.

          Runtime::Env.pipeline_from_project_name
        end
      end
    end
  end
end
