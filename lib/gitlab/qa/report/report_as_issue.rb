# frozen_string_literal: true

module Gitlab
  module QA
    module Report
      class ReportAsIssue
        def initialize(token:, input_files:, project: nil)
          @gitlab = GitlabIssueClient.new(token: token, project: project)
          @files = Array(input_files)
          @project = project
        end

        def invoke!
          validate_input!

          run!
        end

        private

        attr_reader :gitlab, :files, :project

        def run!
          raise NotImplementedError
        end

        def validate_input!
          assert_project!
          assert_input_files!(files)
          gitlab.assert_user_permission!
        end

        def assert_project!
          return if project

          abort "Please provide a valid project ID or path with the `-p/--project` option!"
        end

        def assert_input_files!(files)
          return if Dir.glob(files).any?

          abort "Please provide valid JUnit report files. No files were found matching `#{files.join(',')}`"
        end
      end
    end
  end
end
