# frozen_string_literal: true

module Gitlab
  module QA
    module Report
      class BaseTestResults
        include Enumerable

        def initialize(path)
          @results = parse(path)
          @testcases = process
        end

        def each(&block)
          testcases.each(&block)
        end

        def write(path)
          raise NotImplementedError
        end

        private

        attr_reader :results, :testcases

        def parse(path)
          raise NotImplementedError
        end

        def process
          raise NotImplementedError
        end
      end
    end
  end
end
