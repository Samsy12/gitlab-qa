# frozen_string_literal: true

require 'json'

module Gitlab
  module QA
    module Report
      class JsonTestResults < BaseTestResults
        def write(path)
          json = results.merge('examples' => testcases.map(&:report))

          File.write(path, JSON.pretty_generate(json))
        end

        private

        def parse(path)
          JSON.parse(File.read(path))
        end

        def process
          results['examples'].map do |test|
            TestResult.from_json(test)
          end
        end
      end
    end
  end
end
