# frozen_string_literal: true

require 'nokogiri'

module Gitlab
  module QA
    module Report
      class JUnitTestResults < BaseTestResults
        def write(path)
          # Ignore it for now
        end

        private

        def parse(path)
          Nokogiri::XML.parse(File.read(path))
        end

        def process
          results.xpath('//testcase').map do |test|
            TestResult.from_junit(test)
          end
        end
      end
    end
  end
end
